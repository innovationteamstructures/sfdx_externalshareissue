# SFDX  External_Sharing_Issue Example App

## Issue:  
Using this sample SFDX project custom object external sharing is not being set in the org as defined in the Meta XML of the custom object when performing a Salesforce CLI command  `sfdx force:source:push`  into a new org.

## Reproduce:
1.  Clone this Repo:
2.  Switch to DoesNotWork Branch.
2.  Create a new scratch org from the project-scratch-def in this repo
  * From the root directory of the Repo execute Salesforce CLI command `sfdx force:org:create -f config\project-scratch-def.json -a External_Sharing_Issue` (requires install CLI and have a configured DevHub)
3. Execute a source push into the new scratch org
  *  `sfdx force:source:push -u External_Sharing_Issue`
4.  Note the Push error message: `Invalid type:  ExtPrivateObj__Share` when pushing the ExtPrivateObjHandler class
  *  This is due to the External Sharing on the ExtPrivateObj not being set to Private in the scratch org as defined in the ExtPrivateObj.Meta.xml file (which push without error).
5.  Execute a `sfdx force:org:open` command to open your scratch org
6.  Notice that the ExtPrivateObt external share permissions are Read/Write, not Private.

# Workaround
1.  Switch to the Fix_Step_1 branch.
2.  Execute a source push into the **same** scratch org as before.
  *  Note the Changed ExtPrivateObj__c object pushed and the same error as before
  *  The change was an adding of a desciption to the object.  This was done only to get the SFDX to push the ExtPrivateObj__c.object-meta.xml file again (there are other ways to accomplish).
3.  Open the org `sfdx force:org:open`
4.  Navigate to Sharing Setting in Setup and notice a message indicating that a share change is being processed.  Once complete you will notice the External Permissions are Private.
5.  Re-push the same code 'sfdx force:source:push`
  *  Note:  the Apex classes and the Profile now push successfully.





